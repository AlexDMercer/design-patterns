<?php

use App\Http\Controllers\AbstractFactoryController;
use App\Http\Controllers\AdapterController;
use App\Http\Controllers\BridgeController;
use App\Http\Controllers\BuilderController;
use App\Http\Controllers\ChainController;
use App\Http\Controllers\CommandController;
use App\Http\Controllers\CompositeController;
use App\Http\Controllers\DecoratorController;
use App\Http\Controllers\FacadeController;
use App\Http\Controllers\FactoryMethodController;
use App\Http\Controllers\MediatorController;
use App\Http\Controllers\ObserverController;
use App\Http\Controllers\ProxyController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\StrategyController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\VisitorController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('abstract-factory-1', [AbstractFactoryController::class, 'factory1']);
Route::get('abstract-factory-2', [AbstractFactoryController::class, 'factory2']);

Route::get('adapter', [AdapterController::class, 'adapter']);

Route::get('bridge-1', [BridgeController::class, 'bridgeA']);
Route::get('bridge-2', [BridgeController::class, 'bridgeB']);

Route::get('builder', [BuilderController::class, 'builder']);

Route::get('chain', [ChainController::class, 'chain']);

Route::get('command', [CommandController::class, 'command']);

Route::get('composite-1', [CompositeController::class, 'compositeSimple']);
Route::get('composite-2', [CompositeController::class, 'compositeDifficult']);
Route::get('composite-3', [CompositeController::class, 'severalComponent']);

Route::get('decorator', [DecoratorController::class, 'decorator']);

Route::get('facade', [FacadeController::class, 'facade']);

Route::get('factory-method-1', [FactoryMethodController::class, 'factoryMethodA']);
Route::get('factory-method-2', [FactoryMethodController::class, 'factoryMethodB']);

Route::get('flyweight', [FacadeController::class, 'flyweight']);

Route::get('mediator', [MediatorController::class, 'mediator']);

Route::get('observer', [ObserverController::class, 'observer']);

Route::get('proxy', [ProxyController::class, 'proxy']);

Route::get('state', [StateController::class, 'state']);

Route::get('strategy', [StrategyController::class, 'strategy']);

Route::get('template', [TemplateController::class, 'template']);

Route::get('visitor', [VisitorController::class, 'visitor']);

