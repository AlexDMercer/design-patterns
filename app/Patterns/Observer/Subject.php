<?php

namespace App\Patterns\Observer;

use SplObserver;
use SplSubject;

/**
 * Издатель владеет некоторым важным состоянием и оповещает наблюдателей о его изменениях.
 */
class Subject implements SplSubject
{
    /**
     * Для удобства в этой переменной хранится состояние Издателя, необходимое всем подписчикам.
     */
    public $state;

    /**
     * Список подписчиков. В реальной жизни список
     * подписчиков может храниться в более подробном виде (классифицируется по типу события и т.д.)
     */
    private $observers;

    public function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }

    /**
     * Методы управления подпиской.
     */
    public function attach(SplObserver $observer)
    {
        echo "Subject: Attached an observer.\n";
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->observers->detach($observer);
        echo "Subject: Detached an observer.\n";
    }

    /**
     * Запуск обновления в каждом подписчике.
     */
    public function notify()
    {
        echo "Subject: Notifying observers...\n";
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    /**
     * Обычно логика подписки – только часть того, что делает Издатель.
     * Издатели часто содержат некоторую важную бизнес-логику, которая запускает
     * метод уведомления всякий раз, когда должно произойти что-то важное (или после этого).
     */
    public function someBusinessLogic(): void
    {
        echo "\nSubject: I'm doing something important.\n";
        $this->state = rand(0, 10);

        echo "Subject: My state has just changed to: {$this->state}\n";
        $this->notify();
    }
}
