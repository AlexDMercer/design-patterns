<?php

namespace App\Patterns\Observer;

use SplSubject;

/**
 * Конкретные Наблюдатели реагируют на обновления, выпущенные Издателем, к которому они прикреплены.
 */
class ConcreteObserverB implements \SplObserver
{
    public function update(SplSubject $subject)
    {
        if ($subject->state == 0 || $subject->state >= 2) {
            echo "ConcreteObserverB: Reacted to the event.\n";
        }
    }
}
