<?php

namespace App\Patterns\Chain;

/**
 * Поведение цепочки по умолчанию может быть реализовано внутри базового класса обработчика.
 */
class AbstractHandler implements Handler
{
    private ?Handler $nextHandler = null;

    public function setNext(Handler $handler): Handler
    {
        $this->nextHandler = $handler;

        return $handler;
    }

    public function handle(string $request): ?string
    {
        if ($this->nextHandler) {
            return $this->nextHandler->handle($request);
        }

        return null;
    }
}
