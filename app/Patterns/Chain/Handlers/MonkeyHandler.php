<?php

namespace App\Patterns\Chain\Handlers;

use App\Patterns\Chain\AbstractHandler;

/**
 * Все Конкретные Обработчики либо обрабатывают запрос, либо передают его следующему обработчику в цепочке.
 */
class MonkeyHandler extends AbstractHandler
{
    public function handle(string $request): ?string
    {
        if ($request === "Banana") {
            return "Monkey: I'll eat the " . $request;
        } else {
            return parent::handle($request);
        }
    }
}
