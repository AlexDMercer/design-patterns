<?php

namespace App\Patterns\Chain\Handlers;

use App\Patterns\Chain\AbstractHandler;

/**
 * Все Конкретные Обработчики либо обрабатывают запрос, либо передают его следующему обработчику в цепочке.
 */
class SquirrelHandler extends AbstractHandler
{
    public function handle(string $request): ?string
    {
        if ($request === "Nut") {
            return "Squirrel: I'll eat the " . $request;
        } else {
            return parent::handle($request);
        }
    }
}
