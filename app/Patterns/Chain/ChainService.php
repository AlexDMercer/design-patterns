<?php

namespace App\Patterns\Chain;

/**
 * Обычно клиентский код приспособлен для работы с единственным
 * обработчиком. В большинстве случаев клиенту даже неизвестно, что этот
 * обработчик является частью цепочки.
 */
class ChainService
{
    function clientCode(Handler $handler): array
    {
        $data = [];

        foreach (["Nut", "Banana", "Cup of coffee"] as $food) {
            $result = $handler->handle($food);

            if ($result) {
                $data[] = $result;
            } else {
                $data[] =  $food . " was left untouched";
            }
        }

        return $data;
    }
}
