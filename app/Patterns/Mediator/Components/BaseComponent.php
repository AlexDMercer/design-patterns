<?php

namespace App\Patterns\Mediator\Components;

use App\Patterns\Mediator\Mediator;

/**
 * Базовый Компонент обеспечивает базовую функциональность хранения
 * экземпляра посредника внутри объектов компонентов.
 */
class BaseComponent
{
    public function __construct(protected ?Mediator $mediator = null) {}

    public function setMediator(Mediator $mediator): void
    {
        $this->mediator = $mediator;
    }
}
