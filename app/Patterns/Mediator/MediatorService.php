<?php

namespace App\Patterns\Mediator;

use App\Patterns\Mediator\Components\Component1;
use App\Patterns\Mediator\Components\Component2;

class MediatorService
{
    public function clientCode()
    {
        $c1 = new Component1();
        $c2 = new Component2();
        $mediator = new ConcreteMediator($c1, $c2);

        echo "Client triggers operation A.\n";
        $c1->doA();

        echo "\n";
        echo "Client triggers operation D.\n";
        $c2->doD();
    }
}
