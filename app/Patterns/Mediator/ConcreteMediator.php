<?php

namespace App\Patterns\Mediator;

use App\Patterns\Mediator\Components\Component1;
use App\Patterns\Mediator\Components\Component2;

/**
 * Конкретные Посредники реализуют совместное поведение, координируя отдельные компоненты.
 */
class ConcreteMediator implements Mediator
{
    public function __construct(private Component1 $component1, private Component2 $component2)
    {
        $this->component1->setMediator($this);
        $this->component2->setMediator($this);
    }

    public function notify(object $sender, string $event): void
    {
        if ($event == "A") {
            echo "Mediator reacts on A and triggers following operations:\n";
            $this->component2->doC();
        }

        if ($event == "D") {
            echo "Mediator reacts on D and triggers following operations:\n";
            $this->component1->doB();
            $this->component2->doC();
        }
    }
}
