<?php

namespace App\Patterns\AbstractFactory\Products;

use App\Patterns\AbstractFactory\Factory\AbstractFactory;

class AbstractFactoryService
{
    /**
     * Клиентский код работает с фабриками и продуктами только через абстрактные
     * типы: Абстрактная Фабрика и Абстрактный Продукт. Это позволяет передавать
     * любой подкласс фабрики или продукта клиентскому коду, не нарушая его.
     */
    public function clientCode(AbstractFactory $factory): array
    {
        $productA = $factory->createProductA();
        $productB = $factory->createProductB();

        $result1 = $productB->usefulFunctionB();
        $result2 = $productB->anotherUsefulFunctionB($productA);

        return compact('result1', 'result2');
    }
}
