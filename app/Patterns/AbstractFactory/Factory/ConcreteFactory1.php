<?php

namespace App\Patterns\AbstractFactory\Factory;

use App\Patterns\AbstractFactory\Products\AbstractProductA;
use App\Patterns\AbstractFactory\Products\AbstractProductB;
use App\Patterns\AbstractFactory\Products\ConcreteProductA1;
use App\Patterns\AbstractFactory\Products\ConcreteProductB1;

/**
 * Конкретная Фабрика производит семейство продуктов одной вариации. Фабрика гарантирует совместимость
 * полученных продуктов. Сигнатуры методов Конкретной Фабрики возвращают абстрактный продукт, в то
 * время как внутри метода создается экземпляр конкретного продукта.
 */
class ConcreteFactory1 implements AbstractFactory
{
    public function createProductA(): AbstractProductA
    {
        return new ConcreteProductA1();
    }

    public function createProductB(): AbstractProductB
    {
        return new ConcreteProductB1();
    }
}
