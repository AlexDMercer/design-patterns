<?php

namespace App\Patterns\AbstractFactory\Factory;

use App\Patterns\AbstractFactory\Products\AbstractProductA;
use App\Patterns\AbstractFactory\Products\AbstractProductB;
use App\Patterns\AbstractFactory\Products\ConcreteProductA2;
use App\Patterns\AbstractFactory\Products\ConcreteProductB2;

/**
 * Каждая Конкретная Фабрика имеет соответствующую вариацию продукта.
 */
class ConcreteFactory2 implements AbstractFactory
{
    public function createProductA(): AbstractProductA
    {
        return new ConcreteProductA2();
    }

    public function createProductB(): AbstractProductB
    {
        return new ConcreteProductB2();
    }
}
