<?php

namespace App\Patterns\Command\Commands;

/**
 * Интерфейс Команды объявляет метод для выполнения команд.
 */
interface Command
{
    public function execute(): string;
}

