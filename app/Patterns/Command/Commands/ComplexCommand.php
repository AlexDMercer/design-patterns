<?php

namespace App\Patterns\Command\Commands;

use App\Patterns\Command\Receiver;

/**
 * Есть команды, которые делегируют более сложные операции другим объектам, называемым «получателями».
 */
class ComplexCommand implements Command
{
    public function __construct(
        private Receiver $receiver,
        private string $a,
        private string $b
    ) {}

    /**
     * Команды могут делегировать выполнение любым методам получателя.
     */
    public function execute(): string
    {
        $result = [];

        $result[] = "ComplexCommand: Complex stuff should be done by a receiver object.";
        $result[] = $this->receiver->doSomething($this->a);
        $result[] = $this->receiver->doSomethingElse($this->b);

        return implode(', ', $result);
    }
}
