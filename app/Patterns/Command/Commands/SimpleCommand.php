<?php

namespace App\Patterns\Command\Commands;

/**
 * Некоторые команды способны выполнять простые операции самостоятельно.
 */
class SimpleCommand implements Command
{
    public function __construct(private string $payload) {}

    public function execute(): string
    {
        return "SimpleCommand: See, I can do simple things like printing (" . $this->payload . ")";
    }
}
