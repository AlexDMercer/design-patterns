<?php

namespace App\Patterns\Command;

use App\Patterns\Command\Commands\ComplexCommand;
use App\Patterns\Command\Commands\SimpleCommand;

/**
 * Клиентский код может параметризовать отправителя любыми командами.
 */
class CommandService
{
    public function clientCode(): array
    {
        $invoker  = new Invoker();
        $receiver = new Receiver();
        $invoker->setOnStart(new SimpleCommand("Say Hi!"));
        $invoker->setOnFinish(new ComplexCommand($receiver, "Send email", "Save report"));

        return $invoker->doSomethingImportant();
    }
}
