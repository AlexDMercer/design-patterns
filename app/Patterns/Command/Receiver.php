<?php

namespace App\Patterns\Command;

/**
 * Классы Получателей содержат некую важную бизнес-логику. Они умеют выполнять все виды операций,
 * связанных с выполнением запроса. Фактически, любой класс может выступать Получателем.
 */
class Receiver
{
    public function doSomething(string $a): string
    {
        return "Receiver: Working on (" . $a . ".)";
    }

    public function doSomethingElse(string $b): string
    {
        return "Receiver: Also working on (" . $b . ".)";
    }
}
