<?php

namespace App\Patterns\Command;

use App\Patterns\Command\Commands\Command;

/**
 * Отправитель связан с одной или несколькими командами. Он отправляет запрос команде.
 */
class Invoker
{
    private Command $onStart;
    private Command $onFinish;

    /**
     * Инициализация команд.
     */
    public function setOnStart(Command $command): void
    {
        $this->onStart = $command;
    }

    public function setOnFinish(Command $command): void
    {
        $this->onFinish = $command;
    }

    /**
     * Отправитель не зависит от классов конкретных команд и получателей.
     * Отправитель передаёт запрос получателю косвенно, выполняя команду.
     */
    public function doSomethingImportant(): array
    {
        $result[] =  "Invoker: Does anybody want something done before I begin?";
        $result[] =  $this->onStart->execute();

        $result[] =  "Invoker: ...doing something really important...";

        $result[] =  "Invoker: Does anybody want something done after I finish?";
        $result[] =  $this->onFinish->execute();

        return $result;
    }
}
