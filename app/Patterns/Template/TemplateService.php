<?php

namespace App\Patterns\Template;

/**
 * Клиентский код вызывает шаблонный метод для выполнения алгоритма.
 * Клиентский код не должен знать конкретный класс объекта, с которым работает,
 * при условии, что он работает с объектами через интерфейс их базового класса.
 */
class TemplateService
{
    public function clientCode(AbstractClass $class)
    {
        $class->templateMethod();
    }
}
