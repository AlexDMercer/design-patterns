<?php

namespace App\Patterns\Bridge\Abstraction;

use App\Patterns\Bridge\Realization\Implementation;

/**
 * Абстракция устанавливает интерфейс для «управляющей» части двух иерархий классов.
 * Она содержит ссылку на объект из иерархии Реализации и делегирует ему всю настоящую работу.
 */
class Abstraction
{
    public function __construct(protected Implementation $implementation) {}

    public function operation(): string
    {
        return "Abstraction: Base operation with: " .
            $this->implementation->operationImplementation();
    }
}
