<?php

namespace App\Patterns\Bridge\Abstraction;

/**
 * Можно расширить Абстракцию без изменения классов Реализации.
 */
class ExtendedAbstraction extends Abstraction
{
    public function operation(): string
    {
        return "ExtendedAbstraction: Extended operation with: " .
            $this->implementation->operationImplementation();
    }
}
