<?php

namespace App\Patterns\Bridge;

use App\Patterns\Bridge\Abstraction\Abstraction;

/**
 * За исключением этапа инициализации, когда объект Абстракции связывается с
 * определённым объектом Реализации, клиентский код должен зависеть только от
 * класса Абстракции. Таким образом, клиентский код может поддерживать любую
 * комбинацию абстракции и реализации.
 */
class BridgeService
{
    function clientCode(Abstraction $abstraction): string
    {
        return $abstraction->operation();
    }
}
