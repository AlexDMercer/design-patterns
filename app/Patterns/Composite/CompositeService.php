<?php

namespace App\Patterns\Composite;

/**
 * Клиентский код работает со всеми компонентами через базовый интерфейс.
 */
class CompositeService
{
    public function simpleClientCode(Component $component): string
    {
        return "RESULT: " . $component->operation();
    }

    public function clientCode(Component $component1, Component $component2): string
    {
        if ($component1->isComposite()) {
            $component1->add($component2);
        }

        return "RESULT: " . $component1->operation();
    }
}
