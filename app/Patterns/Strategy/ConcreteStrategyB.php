<?php

namespace App\Patterns\Strategy;

/**
 * Конкретные Стратегии реализуют алгоритм, следуя базовому интерфейсу Стратегии.
 * Этот интерфейс делает их взаимозаменяемыми в Контексте.
 */
class ConcreteStrategyB implements Strategy
{
    public function doAlgorithm(array $data): array
    {
        rsort($data);

        return $data;
    }
}
