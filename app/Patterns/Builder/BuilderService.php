<?php

namespace App\Patterns\Builder;

/**
 * Клиентский код создаёт объект-строитель, передаёт его директору, а затем
 * инициирует процесс построения. Конечный результат извлекается из объекта-строителя.
 */
class BuilderService
{
    public function clientCode(Director $director): array
    {
        $products = [];
        $builder  = new ConcreteBuilder1();
        $director->setBuilder($builder);

        $director->buildMinimalViableProduct();
        $products[] = $builder->getProduct()->listParts();

        $director->buildFullFeaturedProduct();
        $products[] = $builder->getProduct()->listParts();

        $builder->producePartA();
        $builder->producePartC();
        $products[] = $builder->getProduct()->listParts();

        return $products;
    }
}
