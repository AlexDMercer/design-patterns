<?php

namespace App\Patterns\Adapter;

/**
 * Подмена Target на Adapter происходит в сервис провайдере
 */
class AdapterService
{
    public function __construct(private TargetInterface $target) {}

    public function clientCode(): string
    {
        return $this->target->request();
    }
}
