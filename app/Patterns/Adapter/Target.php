<?php

namespace App\Patterns\Adapter;

/**
 * Целевой класс реализует интерфейс, с которым может работать клиентский код.
 */
class Target implements TargetInterface
{
    public function request(): string
    {
        return "Target: The default target's behavior.";
    }
}
