<?php

namespace App\Patterns\Adapter;

/**
 * Адаптер делает интерфейс Адаптируемого класса совместимым с целевым интерфейсом.
 */
class Adapter implements TargetInterface
{
    public function __construct(private Adaptee $adaptee) {}

    public function request(): string
    {
        return "Adapter: (TRANSLATED) " . strrev($this->adaptee->specificRequest());
    }
}
