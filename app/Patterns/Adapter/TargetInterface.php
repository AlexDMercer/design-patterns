<?php

namespace App\Patterns\Adapter;

/**
 * Интерфейс, с которым может работать клиентский код.
 */
interface TargetInterface
{
    public function request(): string;
}
