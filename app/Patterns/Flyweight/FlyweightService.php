<?php

namespace App\Patterns\Flyweight;

/**
 * Клиентский код либо сохраняет, либо вычисляет внешнее состояние и передает его методам легковеса.
 */
class FlyweightService
{
    function addCarToPoliceDatabase(FlyweightFactory $ff, $plates, $owner, $brand, $model, $color) {
        echo "\nClient: Adding a car to database.\n";
        $flyweight = $ff->getFlyweight([$brand, $model, $color]);

        $flyweight->operation([$plates, $owner]);
    }

}
