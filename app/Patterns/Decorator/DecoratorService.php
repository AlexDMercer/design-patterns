<?php

namespace App\Patterns\Decorator;

/**
 * Клиентский код работает со всеми объектами, используя интерфейс Компонента.
 * Таким образом, он остаётся независимым от конкретных классов компонентов, с которыми работает.
 */
class DecoratorService
{
    public function clientCode(Component $component): string
    {
        return "RESULT: " . $component->operation();
    }
}
