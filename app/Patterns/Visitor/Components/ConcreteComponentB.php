<?php

namespace App\Patterns\Visitor\Components;

use App\Patterns\Visitor\Visitors\Visitor;

/**
 * То же самое здесь: visitConcreteComponentB => ConcreteComponentB
 */
class ConcreteComponentB implements Component
{
    public function accept(Visitor $visitor): void
    {
        $visitor->visitConcreteComponentB($this);
    }

    public function specialMethodOfConcreteComponentB(): string
    {
        return "B";
    }
}
