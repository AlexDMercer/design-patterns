<?php

namespace App\Patterns\Visitor\Components;

use App\Patterns\Visitor\Visitors\Visitor;

/**
 * Интерфейс Компонента объявляет метод accept, который в качестве аргумента
 * может получать любой объект, реализующий интерфейс посетителя.
 */
interface Component
{
    public function accept(Visitor $visitor): void;
}
