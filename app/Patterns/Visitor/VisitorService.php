<?php

namespace App\Patterns\Visitor;

use App\Patterns\Visitor\Visitors\Visitor;

/**
 * Клиентский код может выполнять операции посетителя над любым набором
 * элементов, не выясняя их конкретных классов. Операция принятия направляет
 * вызов к соответствующей операции в объекте посетителя.
 */
class VisitorService
{
    function clientCode(array $components, Visitor $visitor)
    {
        foreach ($components as $component) {
            $component->accept($visitor);
        }
    }
}
