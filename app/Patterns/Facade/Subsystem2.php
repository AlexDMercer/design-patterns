<?php

namespace App\Patterns\Facade;

/**
 * Некоторые фасады могут работать с разными подсистемами одновременно.
 */
class Subsystem2
{
    public function operation1(): string
    {
        return "Subsystem2: Get ready!";
    }

    public function operationB(): string
    {
        return "Subsystem2: Fire!";
    }
}
