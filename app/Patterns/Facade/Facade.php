<?php

namespace App\Patterns\Facade;

/**
 * Класс Фасада предоставляет простой интерфейс для сложной логики одной или
 * нескольких подсистем. Фасад делегирует запросы клиентов соответствующим
 * объектам внутри подсистемы. Фасад также отвечает за управление их жизненным
 * циклом. Все это защищает клиента от нежелательной сложности подсистемы.
 */
class Facade
{
    /**
     * В зависимости от потребностей вашего приложения вы можете предоставить Фасаду
     * существующие объекты подсистемы или заставить Фасад создать их самостоятельно.
     */
    public function __construct(protected ?Subsystem1 $subsystem1 = null, protected ?Subsystem2 $subsystem2 = null) {}

    /**
     * Методы Фасада удобны для быстрого доступа к сложной функциональности подсистем.
     * Однако клиенты получают только часть возможностей подсистемы.
     */
    public function operation(): string
    {
        $result = "Facade initializes subsystems: ";
        $result .= $this->subsystem1->operation1();
        $result .= $this->subsystem2->operation1();
        $result .= "Facade orders subsystems to perform the action: ";
        $result .= $this->subsystem1->operationA();
        $result .= $this->subsystem2->operationB();

        return $result;
    }
}
