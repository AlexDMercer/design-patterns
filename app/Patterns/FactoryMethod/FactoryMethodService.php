<?php

namespace App\Patterns\FactoryMethod;

/**
 * Клиентский код работает с экземпляром конкретного создателя, хотя и через
 * его базовый интерфейс. Пока клиент продолжает работать с создателем через
 * базовый интерфейс, вы можете передать ему любой подкласс создателя.
 */
class FactoryMethodService
{
    public function clientCode(Creator $creator): string
    {
        return "Client: I'm not aware of the creator's class, but it still works. " . $creator->someOperation();
    }
}
