<?php

namespace App\Patterns\FactoryMethod;

use App\Patterns\FactoryMethod\Products\ConcreteProduct2;
use App\Patterns\FactoryMethod\Products\Product;

/**
 * Конкретные Создатели переопределяют фабричный метод для того, чтобы изменить тип результирующего продукта.
 */
class ConcreteCreator2 extends Creator
{

    /**
     * Сигнатура метода по-прежнему использует тип абстрактного продукта,
     * хотя фактически из метода возвращается конкретный продукт.
     * Таким образом, Создатель может оставаться независимым от конкретных классов продуктов.
     */
    public function factoryMethod(): Product
    {
        return new ConcreteProduct2();
    }
}
