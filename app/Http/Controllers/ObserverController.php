<?php

namespace App\Http\Controllers;

use App\Patterns\Observer\ObserverService;

/**
 * Паттерн Наблюдатель
 *
 * Назначение: Создаёт механизм подписки, позволяющий одним объектам следить и
 * реагировать на события, происходящие в других объектах.
 */
class ObserverController extends Controller
{
    public function __construct(private ObserverService $service) {}

    public function observer()
    {
        $this->service->observer();
    }
}
