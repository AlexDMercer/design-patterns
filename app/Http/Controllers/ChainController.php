<?php

namespace App\Http\Controllers;

use App\Patterns\Chain\ChainService;
use App\Patterns\Chain\Handlers\DogHandler;
use App\Patterns\Chain\Handlers\MonkeyHandler;
use App\Patterns\Chain\Handlers\SquirrelHandler;

/**
 * Паттерн Цепочка обязанностей
 *
 * Назначение: Позволяет передавать запросы последовательно по цепочке
 * обработчиков. Каждый последующий обработчик решает, может ли он обработать
 * запрос сам и стоит ли передавать запрос дальше по цепи.
 */
class ChainController extends Controller
{
    public function __construct(private ChainService $service) {}

    /**
     * Другая часть клиентского кода создает саму цепочку.
     */
    public function chain()
    {
        $monkey   = new MonkeyHandler();
        $squirrel = new SquirrelHandler();
        $dog      = new DogHandler();

        $monkey->setNext($squirrel)->setNext($dog);

        return $this->service->clientCode($monkey);
    }
}
