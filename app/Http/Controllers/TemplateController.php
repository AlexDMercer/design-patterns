<?php

namespace App\Http\Controllers;

use App\Patterns\Template\ConcreteClass1;
use App\Patterns\Template\ConcreteClass2;
use App\Patterns\Template\TemplateService;

/**
 * Паттерн Шаблонный метод
 *
 * Назначение: Определяет общую схему алгоритма, перекладывая реализацию
 * некоторых шагов на подклассы. Шаблонный метод позволяет подклассам
 * переопределять отдельные шаги алгоритма без изменения структуры алгоритма.
 */
class TemplateController extends Controller
{
    public function __construct(private TemplateService $service) {}

    public function template()
    {
        echo "Same client code can work with different subclasses:\n";
        $this->service->clientCode(new ConcreteClass1());

        echo "Same client code can work with different subclasses:\n";
        $this->service->clientCode(new ConcreteClass2());
    }
}
