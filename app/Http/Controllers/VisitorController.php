<?php

namespace App\Http\Controllers;

use App\Patterns\Visitor\Components\ConcreteComponentA;
use App\Patterns\Visitor\Components\ConcreteComponentB;
use App\Patterns\Visitor\Visitors\ConcreteVisitor1;
use App\Patterns\Visitor\Visitors\ConcreteVisitor2;
use App\Patterns\Visitor\VisitorService;

/**
 * Паттерн Посетитель
 *
 * Назначение: Позволяет создавать новые операции, не меняя классы объектов, над
 * которыми эти операции могут выполняться.
 */
class VisitorController extends Controller
{
    public function __construct(private VisitorService $service) {}

    public function visitor()
    {
        $components = [
            new ConcreteComponentA(),
            new ConcreteComponentB(),
        ];

        echo "The client code works with all visitors via the base Visitor interface:\n";
        $visitor1 = new ConcreteVisitor1();
        $this->service->clientCode($components, $visitor1);
        echo "\n";

        echo "It allows the same client code to work with different types of visitors:\n";
        $visitor2 = new ConcreteVisitor2();
        $this->service->clientCode($components, $visitor2);
    }
}
