<?php

namespace App\Http\Controllers;

use App\Patterns\FactoryMethod\ConcreteCreator1;
use App\Patterns\FactoryMethod\ConcreteCreator2;
use App\Patterns\FactoryMethod\FactoryMethodService;

/**
 * Паттерн Фабричный Метод
 *
 * Назначение: Определяет общий интерфейс для создания объектов в суперклассе,
 * позволяя подклассам изменять тип создаваемых объектов.
 */
class FactoryMethodController extends Controller
{
    public function __construct(private FactoryMethodService $service) {}

    /**
     * Приложение выбирает тип создателя в зависимости от конфигурации или среды.
     */
    public function factoryMethodA(): string
    {
        $creator = new ConcreteCreator1();

        return $this->service->clientCode($creator);
    }

    /**
     * Приложение выбирает тип создателя в зависимости от конфигурации или среды.
     */
    public function factoryMethodB(): string
    {
        $creator = new ConcreteCreator2();

        return $this->service->clientCode($creator);
    }
}
