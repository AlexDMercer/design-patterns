<?php

namespace App\Http\Controllers;

use App\Patterns\Decorator\ConcreteComponent;
use App\Patterns\Decorator\ConcreteDecoratorA;
use App\Patterns\Decorator\ConcreteDecoratorB;
use App\Patterns\Decorator\DecoratorService;

/**
 * Паттерн Декоратор
 *
 * Назначение: Позволяет динамически добавлять объектам новую функциональность, оборачивая их в полезные «обёртки».
 */
class DecoratorController extends Controller
{
    public function __construct(private DecoratorService $service) {}

    /**
     * Декораторы могут обёртывать не только простые компоненты, но и другие декораторы.
     */
    public function decorator(): string
    {
        $simple     = new ConcreteComponent();

        $decorator1 = new ConcreteDecoratorA($simple);

//        dd($decorator1);

        $decorator2 = new ConcreteDecoratorB($decorator1);

        return $this->service->clientCode($decorator2);
    }
}
