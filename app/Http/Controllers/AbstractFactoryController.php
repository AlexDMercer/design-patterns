<?php

namespace App\Http\Controllers;

use App\Patterns\AbstractFactory\Factory\ConcreteFactory1;
use App\Patterns\AbstractFactory\Factory\ConcreteFactory2;
use App\Patterns\AbstractFactory\Products\AbstractFactoryService;
use Illuminate\Http\JsonResponse;

/**
 * Назначение: Предоставляет интерфейс для создания семейств связанных или
 * зависимых объектов без привязки к их конкретным классам.
 *
 * Интерфейс Абстрактной Фабрики объявляет набор методов, которые возвращают
 * различные абстрактные продукты. Эти продукты называются семейством и связаны
 * темой или концепцией высокого уровня. Продукты одного семейства обычно могут
 * взаимодействовать между собой. Семейство продуктов может иметь несколько
 * вариаций, но продукты одной вариации несовместимы с продуктами другой.
 */
class AbstractFactoryController extends Controller
{
    public function __construct(private AbstractFactoryService $service) {}

    /**
     * Клиентский код может работать с любым конкретным классом фабрики.
     */
    public function factory1(): JsonResponse
    {
        return response()->json($this->service->clientCode(new ConcreteFactory1()));
    }

    /**
     * Клиентский код может работать с любым конкретным классом фабрики.
     */
    public function factory2(): JsonResponse
    {
        return response()->json($this->service->clientCode(new ConcreteFactory2()));

    }
}
