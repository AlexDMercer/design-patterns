<?php

namespace App\Http\Controllers;

use App\Patterns\Composite\Composite;
use App\Patterns\Composite\CompositeService;
use App\Patterns\Composite\Leaf;

/**
 * Паттерн Компоновщик
 *
 * Назначение: Позволяет сгруппировать объекты в древовидную структуру, а затем
 * работать с ними так, как будто это единичный объект.
 */
class CompositeController extends Controller
{
    public function __construct(private CompositeService $service) {}

    /**
     * клиентский код может поддерживать простые компоненты-листья...
     */
    public function compositeSimple(): string
    {
        $simple = new Leaf();

        return $this->service->simpleClientCode($simple);
    }

    /**
     * а также сложные контейнеры.
     */
    public function compositeDifficult(): string
    {
        $tree    = new Composite();
        $branch1 = new Composite();
        $branch1->add(new Leaf());
        $branch1->add(new Leaf());
        $branch2 = new Composite();
        $branch2->add(new Leaf());
        $tree->add($branch1);
        $tree->add($branch2);

        return $this->service->simpleClientCode($tree);
    }

    /**
     * Благодаря тому, что операции управления потомками объявлены в базовом
     * классе Компонента, клиентский код может работать как с простыми, так и со
     * сложными компонентами, вне зависимости от их конкретных классов.
     */
    public function severalComponent(): string
    {
        $simple = new Leaf();
        $tree   = new Composite();

        return $this->service->clientCode($simple, $tree);
    }
}
