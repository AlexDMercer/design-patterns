<?php

namespace App\Http\Controllers;

use App\Patterns\Singleton\Singleton;

/**
 * Паттерн Одиночка
 *
 * Назначение: Гарантирует, что у класса есть только один экземпляр, и
 * предоставляет к нему глобальную точку доступа.
 */
class SingletonController extends Controller
{
    public function singleton()
    {
        $s1 = Singleton::getInstance();
        $s2 = Singleton::getInstance();

        if ($s1 === $s2) {
            echo "Singleton works, both variables contain the same instance.";
        } else {
            echo "Singleton failed, variables contain different instances.";
        }
    }
}
