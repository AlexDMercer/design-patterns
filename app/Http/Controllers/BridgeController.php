<?php

namespace App\Http\Controllers;

use App\Patterns\Bridge\Abstraction\Abstraction;
use App\Patterns\Bridge\Abstraction\ExtendedAbstraction;
use App\Patterns\Bridge\BridgeService;
use App\Patterns\Bridge\Realization\ConcreteImplementationA;
use App\Patterns\Bridge\Realization\ConcreteImplementationB;

/**
 * Паттерн Мост
 *
 * Разделяет один или несколько классов на две отдельные иерархии —
 * абстракцию и реализацию, позволяя изменять их независимо друг от друга.
 */
class BridgeController extends Controller
{
    public function __construct(private BridgeService $service) {}

    public function bridgeA(): string
    {
        $implementation = new ConcreteImplementationA();
        $abstraction    = new Abstraction($implementation);

        return $this->service->clientCode($abstraction);
    }

    public function bridgeB(): string
    {
        $implementation = new ConcreteImplementationB();
        $abstraction    = new ExtendedAbstraction($implementation);

        return $this->service->clientCode($abstraction);
    }
}
