<?php

namespace App\Http\Controllers;

use App\Patterns\Strategy\ConcreteStrategyA;
use App\Patterns\Strategy\ConcreteStrategyB;
use App\Patterns\Strategy\Context;

/**
 * Паттерн Стратегия
 *
 * Назначение: Определяет семейство схожих алгоритмов и помещает каждый из них в
 * собственный класс, после чего алгоритмы можно взаимозаменять прямо во время исполнения программы.
 */
class StrategyController extends Controller
{
    public function strategy()
    {
        $context = new Context(new ConcreteStrategyA());
        echo "Client: Strategy is set to normal sorting.\n";
        $context->doSomeBusinessLogic();

        echo "\n";

        echo "Client: Strategy is set to reverse sorting.\n";
        $context->setStrategy(new ConcreteStrategyB());
        $context->doSomeBusinessLogic();
    }
}
