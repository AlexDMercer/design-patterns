<?php

namespace App\Http\Controllers;

use App\Patterns\Command\CommandService;

/**
 * Паттерн Команда
 *
 * Назначение: Превращает запросы в объекты, позволяя передавать их как
 * аргументы при вызове методов, ставить запросы в очередь, логировать их, а
 * также поддерживать отмену операций.
 */
class CommandController extends Controller
{
    public function __construct(private CommandService $service) {}

    public function command(): array
    {
        return $this->service->clientCode();
    }
}
