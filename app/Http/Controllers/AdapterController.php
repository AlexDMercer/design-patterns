<?php

namespace App\Http\Controllers;

use App\Patterns\Adapter\AdapterService;

/**
 * Паттерн Адаптер
 * Позволяет объектам с несовместимыми интерфейсами работать вместе.
 */
class AdapterController extends Controller
{
    public function __construct(private AdapterService $service) {}

    public function adapter()
    {
        return $this->service->clientCode();
    }
}
