<?php

namespace App\Http\Controllers;

use App\Patterns\Facade\Facade;
use App\Patterns\Facade\FacadeService;
use App\Patterns\Facade\Subsystem1;
use App\Patterns\Facade\Subsystem2;

/**
 * Паттерн Фасад
 *
 * Назначение: Предоставляет простой интерфейс к сложной системе классов, библиотеке или фреймворку.
 */
class FacadeController extends Controller
{
    public function __construct(private FacadeService $service) {}

    /**
     * В клиентском коде могут быть уже созданы некоторые объекты подсистемы. В
     * этом случае может оказаться целесообразным инициализировать Фасад с этими
     * объектами вместо того, чтобы позволить Фасаду создавать новые экземпляры.
     */
    public function facade(): string
    {
        $subsystem1 = new Subsystem1();
        $subsystem2 = new Subsystem2();
        $facade     = new Facade($subsystem1, $subsystem2);

        return $this->service->clientCode($facade);
    }
}
