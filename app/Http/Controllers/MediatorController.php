<?php

namespace App\Http\Controllers;

use App\Patterns\Mediator\MediatorService;

/**
 * Паттерн Посредник
 *
 * Назначение: Позволяет уменьшить связанность множества классов между собой,
 * благодаря перемещению этих связей в один класс-посредник.
 */
class MediatorController extends Controller
{
    public function __construct(private MediatorService $service) {}

    public function mediator()
    {
        $this->service->clientCode();
    }
}
