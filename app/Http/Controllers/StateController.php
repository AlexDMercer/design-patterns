<?php

namespace App\Http\Controllers;

use App\Patterns\State\ConcreteStateA;
use App\Patterns\State\Context;

/**
 * Паттерн Состояние
 *
 * Назначение: Позволяет объектам менять поведение в зависимости от своего
 * состояния. Извне создаётся впечатление, что изменился класс объекта.
 */
class StateController extends Controller
{
    public function state()
    {
        $context = new Context(new ConcreteStateA());

        $context->request1();
        $context->request2();
    }
}
