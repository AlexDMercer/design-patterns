<?php

namespace App\Http\Controllers;

use App\Patterns\Proxy\Proxy;
use App\Patterns\Proxy\ProxyService;
use App\Patterns\Proxy\RealSubject;

/**
 * Паттерн Заместитель
 *
 * Назначение: Позволяет подставлять вместо реальных объектов специальные
 * объекты-заменители. Эти объекты перехватывают вызовы к оригинальному объекту,
 * позволяя сделать что-то до или после передачи вызова оригиналу.
 */
class ProxyController extends Controller
{
    public function __construct(private ProxyService $service) {}

    public function proxy()
    {
        echo "Client: Executing the client code with a real subject:\n";
        $realSubject = new RealSubject();
        $this->service->clientCode($realSubject);

        echo "\n";

        echo "Client: Executing the same client code with a proxy:\n";
        $proxy = new Proxy($realSubject);
        $this->service->clientCode($proxy);

    }
}
