<?php

namespace App\Http\Controllers;

use App\Patterns\Flyweight\FlyweightFactory;
use App\Patterns\Flyweight\FlyweightService;

/**
 * Паттерн Легковес
 *
 * Назначение: Позволяет вместить бóльшее количество объектов в отведённую
 * оперативную память. Легковес экономит память, разделяя общее состояние
 * объектов между собой, вместо хранения одинаковых данных в каждом объекте.
 */
class FlyweightController extends Controller
{
    public function __construct(private FlyweightService $service) {}

    public function flyweight()
    {
        $factory = new FlyweightFactory([
            ["Chevrolet", "Camaro2018", "pink"],
            ["Mercedes Benz", "C300", "black"],
            ["Mercedes Benz", "C500", "red"],
            ["BMW", "M5", "red"],
            ["BMW", "X6", "white"],
        ]);

        $factory->listFlyweights();

        $this->service->addCarToPoliceDatabase($factory, "CL234IR", "James Doe", "BMW", "M5", "red");
        $this->service->addCarToPoliceDatabase($factory, "CL234IR", "James Doe", "BMW", "X1", "red");
    }
}
