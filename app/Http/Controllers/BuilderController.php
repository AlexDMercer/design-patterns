<?php

namespace App\Http\Controllers;

use App\Patterns\Builder\BuilderService;
use App\Patterns\Builder\Director;

/**
 * Паттерн Строитель
 *
 * Позволяет создавать сложные объекты пошагово. Строитель даёт
 * возможность использовать один и тот же код строительства для получения разных
 * представлений объектов.
 */
class BuilderController extends Controller
{
    public function __construct(private BuilderService $service) {}

    public function builder(): array
    {
        $director = new Director();

        return $this->service->clientCode($director);
    }
}
