<?php

namespace App\Providers;

use App\Patterns\Adapter\Adapter;
use App\Patterns\Adapter\Target;
use App\Patterns\Adapter\TargetInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
//===== Adapter =====
//        TargetInterface::class => Target::class,
        TargetInterface::class => Adapter::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
